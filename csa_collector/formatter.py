import collections
from cpem import CPEMatcher
from stix2 import Software, ObservedData
import datetime

"""
type (required) 
value (required)
resolves_to_refs (optional) - can be a  ipv4-addr or ipv6-addr or domain-name
"""
DomainName = collections.namedtuple('DomainName', 'type value resolves_to_refs')

"""
type (required) - can be ipv4-addr or ipv6-addr accordingly
value (required)
resolves_to_refs (optional)
belongs_to_refs (optional)
"""
IPAddress = collections.namedtuple('IPAddress', 'type value resolves_to_refs belongs_to_refs')


class STIXFormatter:
    def __init__(self):
        self.cpe_matcher = CPEMatcher()
        pass

    def do(self, host_data):
        hardware = host_data['hardware']
        cpe_proc = self.cpe_matcher.im_lucky(description=hardware['proc_model'])
        ip = IPAddress('ipv4-addr', host_data['ipv4_addr'], None, None)
        domain = DomainName('domain-name', None, ip)

        objects = dict()
        for i, so in enumerate(host_data['packages']):
            obj = self.__format_software_object(so)
            objects[str(i)] = obj

        now = datetime.datetime.now()
        return ObservedData(objects=objects,
                            number_observed=1,
                            first_observed=str(now),
                            last_observed=str(now))

    def __format_software_object(self, software_object):
        cpe = self.cpe_matcher.im_lucky(description='1000guess') # software_object['description'])
        return Software(name=software_object['name'],
                        cpe=cpe.new_cpe,
                        vendor=software_object['vendor'],
                        version=software_object['version'])
