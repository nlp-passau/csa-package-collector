from flask import Flask, request, Response
import requests
from csa_collector.formatter import STIXFormatter
import argparse
import time

MESSAGE = '{"hardware": {"proc_vendor": "GenuineIntel", "proc_family": "6", "memory_size": 16721420288, "num_proc": 4,"proc_model": "Intel(R) Core(TM) i7-4600U CPU @ 2.10GHz"}, "lpa": {"name": "test lpa", "id": "777"},"ipv4_addr": "130.82.117.190", "packages": [{"version": "5.3.0-2", "name": "fonts-linuxlibertine","description": "", "vendor": ""}, {"version": "20160321ubuntu1", "name": "ca-certificates-java","description": "", "vendor": ""}, {"version": "0.8.3-1ubuntu3", "name": "speech-dispatcher-audio-plugins","description": "", "vendor": ""}, {"version": "1:5.1.0-1ubuntu2.2", "name": "hunspell-en-gb","description": "", "vendor": ""}, {"version": "1.3.23-1ubuntu0.1", "name": "libgraphicsmagick-q16-3","description": "", "vendor": ""}, {"version": "1.1.8-3.2ubuntu2.1", "name": "libpam0g", "description": "","vendor": ""}, {"version": "15.10.8", "name": "ubuntu-settings", "description": "", "vendor": ""},{"version": "1.0.3-1", "name": "libmpc3", "description": "", "vendor": ""},{"version": "1.5.58ubuntu1", "name": "debconf", "description": "", "vendor": ""},{"version": "1.44-1ubuntu1", "name": "liblog-log4perl-perl", "description": "", "vendor": ""},{"version": "1.7~git20150920+dfsg-4ubuntu1.16.04.1", "name": "libgssapi3-heimdal","description": "", "vendor": ""}]}'

app = Flask(__name__)
formatter = STIXFormatter()
url = None


@app.route('/print', methods=['POST'])
def print_package():
    print(request.json)
    return Response(status=200)


@app.route('/forward', methods=['POST'])
def forward():
    host_data = request.json
    in_stix = formatter.do(host_data)
    post_response = requests.post(url, data=in_stix)

    return Response(status=post_response.status_code)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--url", help="set url to send by HTTP-POST the STIX formatted files.")
    parser.add_argument("-d", "--debug", help="set debug mode.", action="store_true")
    parser.add_argument("-t", "--test", help="sends mocked messages to the endpoint each 10 sec for 1 min.",
                        action="store_true")

    args = parser.parse_args()
    url = args.url

    print(args)

    if not args.test:
        app.run(debug=args.debug, port=5000)
    else:
        import json

        for i in range(6):
            print('sending forward {}'.format(i))
            json_message = json.loads(MESSAGE)
            in_stix = formatter.do(json_message)
            print(str(in_stix))
            post_response = requests.post(url, data=str(in_stix))
            time.sleep(10)