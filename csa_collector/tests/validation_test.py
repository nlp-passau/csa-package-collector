import unittest
import stix2validator
from csa_collector.formatter import STIXFormatter
from csa_collector.service import MESSAGE
import json

class TestValidation(unittest.TestCase):
    def test_message_validation(self):
        json_message = json.loads(MESSAGE)

        formatter = STIXFormatter()
        stix_message = formatter.do(json_message)
        results = stix2validator.validate_string(str(stix_message))

        if not results.is_valid:
            stix2validator.print_results(results)
            self.fail()

